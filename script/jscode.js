const images = document.querySelectorAll('.image-to-show');
let stop = document.getElementsByClassName('stop button')[0];
let start = document.getElementsByClassName('start button')[0];
let time = document.getElementsByClassName('time')[0];
let imgNumber = 0;

const tt = time.innerText;

for (let i = 0; i < images.length; i++) {
  images[i].style.display = 'none'
}

start.style.display = 'none'
images[0].style.display = '';


function changeImg() {

  images[imgNumber].style.display = 'none';

  if (imgNumber < images.length - 1) {
    images[imgNumber + 1].style.display = '';
  } else if (imgNumber === images.length - 1) {
    images[0].style.display = '';
  }

  imgNumber++;

  if (imgNumber === images.length) {
    imgNumber = 0
  };

};

function timeChange() {
  time.innerText = `${(time.innerText - 0.1).toFixed(1)}`
}

let timer = setInterval(timeChange, 100);

let tim = setInterval(() => {
  changeImg();
  time.innerText = tt
}, tt*1000);

stop.onclick = () => {
  clearInterval(tim)
  clearInterval(timer)
  start.style.display = ''
  time.innerText = tt;
  images.forEach(function(element) {
    element.style.animation = 'none'
  });
};

start.onclick = () => {
  tim = setInterval(() => {
    changeImg();
    time.innerText = tt
  }, tt*1000);
  timer = setInterval(timeChange, 100);
  start.style.display = 'none';
  images.forEach(function(element) {
    element.style.animation = ''
  });
};